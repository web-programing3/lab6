import {
  Controller,
  Delete,
  Get,

} from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }

  @Get('hello')
  getHello(): string {
    return '<html><h1>Hello Buu</h1></html>';
  }

  @Delete('world')
  getWorld(): string {
    return '<html><h1>Buu World</h1></html>';
  }
}
